class Dog extends Animal {
    constructor(food, location, size) {
        super(food, location);
        this.size = size;
    }

    makeNoise() {
        console.log("Гав-гав");
    }

    eat() {
        console.log("Собака їсть");
    }
}

class Cat extends Animal {
    constructor(food, location, breed) {
        super(food, location);
        this.breed = breed;
    }

    makeNoise() {
        console.log("Мяу-мяу");
    }

    eat() {
        console.log("Кошка їсть");
    }
}

class Horse extends Animal {
    constructor(food, location, color) {
        super(food, location);
        this.color = color;
    }

    makeNoise() {
        console.log("Іго-го");
    }

    eat() {
        console.log("Кінь Їсть");
    }
}

class Vet {
    treatAnimal(animal) {
        console.log(`Прийшла тварина  що їсть ${animal.food} та живе у ${animal.location}`);
    }
}

console.log("-------");
console.log("Task 5:");

let cat = new Cat(`Dry food`, `Flat`, `British`);
let horse = new Horse(`Oats`, `Stable`, `White`);
let dog = new Dog(`Meat`, `Yard`, `Shepherd dog`);
let animals = [cat, horse, dog];

let i = 0;
let vet = new Vet();
for (i = 0; i < animals.length; i++) {
    vet.treatAnimal(animals[i]);
    animals[i].makeNoise();
}