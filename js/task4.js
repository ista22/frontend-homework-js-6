class Person {
    constructor(fullName, age) {
        this.age = age;
        this.fullName = fullName;
    }

    toString() {
        return `${this.fullName}, ${this.age}`;
    }
}

class Driver extends Person {
    constructor(fullName, age, experience) {
        super(fullName, age);
        this.experience = experience;
    }

    toString() {
        return `Driver ${super.toString()}, ${this.experience} years of experience`;
    }
}

class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }

    toString() {
        return `Engine ${this.power}hp by ${this.company}`;
    }
}

class Car {
    constructor(carClass, engine, driver, model) {
        this.carClass = carClass;
        this.engine = engine;
        this.driver = driver;
        this.model = model;
    }

    start() {
        console.log("Поїхали");
    }

    stop() {
        console.log("Зупиняємося");
    }

    turnRight() {
        console.log("Поворот праворуч");
    }

    turnLeft() {
        console.log("Поворот ліворуч");
    }

    toString() {
        return `Car ${this.model} of class ${this.carClass}, ${this.engine}, is driven by ${this.driver}`;
    }
}

class Lorry extends Car {
    constructor(carClass, engine, driver, model, carrying) {
        super(carClass, engine, driver, model);
        this.carrying = carrying;
    }

    toString() {
        return `Lorry ${this.model} of class ${this.carClass}, ${this.engine}, \
with ${this.carrying}kg carrying, is driven by ${this.driver}`;
    }
}
class Sportcar extends Car {
    constructor(carClass, engine, driver, model, speed) {
        super(carClass, engine, driver, model);
        this.speed = speed;
    }

    toString() {
        return `Sportcar ${this.model} of class ${this.carClass}, ${this.engine}, \
with maximum speed of ${this.speed}km/h, is driven by ${this.driver}`;
    }
}

console.log("-------");
console.log("Task 4:");

let driver = new Driver("Pavlo Bondarenko", 25, 3);
let car = new Car('Medium', new Engine(100, "Nissan"), driver, "Nissan Juke");
let lorry = new Lorry("Lorry", new Engine(450, "Mercedes"), driver, "Mercedes Actros F", 20000);
let sportcar = new Sportcar("Sport coupe", new Engine(480, "Ferrari"), driver, "Ferrari F40", 320);

console.log("Driving a car...");
car.start();
car.turnRight();
car.turnLeft();
car.stop();

console.log("Driving a lorry...");
lorry.start();
lorry.turnRight();
lorry.stop();

console.log("Driving a sportcar...");
sportcar.start();
sportcar.turnLeft();
sportcar.stop();

console.log("Testing .toString():");
console.log(driver.toString());
console.log(car.toString());
console.log(lorry.toString());
console.log(sportcar.toString());

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }

    makeNoise() {
        console.log("Ця тваріна видає звук");
    }

    eat() {
        console.log("Ця тваріна Їсть");
    }

    sleep() {
        console.log("Така тварина спить");
    }
}