class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

    receiveCall(name) {
        console.log("Телефонує ", name);
    }

    getNumber() {
        return this.number;
    }
}

console.log("-------");
console.log("Task 2:");
let phone1 = new Phone(567890, "Samsung", 200);
let phone2 = new Phone(567382, "Nokia", 150);
let phone3 = new Phone(236238, "Apple", 230);

phone1.receiveCall("Pavel");
console.log("Номер ", phone1.getNumber());
phone2.receiveCall("Anton");
console.log("Номер ", phone2.getNumber());
phone3.receiveCall("Maria");
console.log("Номер ", phone3.getNumber());