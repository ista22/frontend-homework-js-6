class MyString {
    reverse(line) {
        let reversed = '';
        for (let i = line.length - 1; i >= 0; i--) {
            reversed += line.charAt(i);
        }
        return reversed;
    }

    ucFirst(line) {
        return line.charAt(0).toUpperCase() + line.slice(1);
    }

    ucWords(line) {
        return line.split(' ').filter(word => word.length != 0).map(this.ucFirst).join(' ');
    }
}

console.log("-------");
console.log("Task 3:");

let myString = new MyString();
console.log(`reverse('book'): ${myString.reverse('book')}`);
console.log(`ucFirst('book'): ${myString.ucFirst('book')}`);
console.log(`ucWords('hello world'): ${myString.ucWords('hello world')}`);