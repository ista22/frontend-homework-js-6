class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }

    getSalary() {
        let getSalary = this.rate * this.days;
        return getSalary;
    }
}

console.log("-------");
console.log("Task 1:");

let worker1 = new Worker("Taras", "Ivanov", 500, 5);
let salary = worker1.getSalary();
console.log(`Worker ${worker1.name} ${worker1.surname} has salary ${salary}`);